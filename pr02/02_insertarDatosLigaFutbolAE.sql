-- Alberto de la Encina Vara modificado del original de Virginia Francisco Gilmart�n

----- PERSONAS -------
INSERT INTO Futbol_personas VALUES
('11111111A', 'David P�rez Pallas');

INSERT INTO Futbol_personas VALUES
('22222222B', 'Alexandre Alem�n P�rez');

INSERT INTO Futbol_personas VALUES
('33333333C', 'Mois�s Mateo MMta��s');

INSERT INTO Futbol_personas VALUES
('44444444D', 'Adri�n D�az Gonz�lez');

INSERT INTO Futbol_personas VALUES
('55555555E', 'Juan Manuel L�pez Amaya');

INSERT INTO Futbol_personas VALUES
('66666666F', 'Iv�n Gonz�lez Gonz�lez');

INSERT INTO Futbol_personas VALUES
('77777777G', 'Jorge Figueroa V�zquez');

INSERT INTO Futbol_personas VALUES
('11111110A', 'Ronaldo');
INSERT INTO Futbol_personas VALUES
('22222221B', 'Villa');
INSERT INTO Futbol_personas VALUES
('33333332C', 'Messi');
INSERT INTO Futbol_personas VALUES
('44444443D', 'Ra�l');
INSERT INTO Futbol_personas VALUES
('55555554E', 'David');
INSERT INTO Futbol_personas VALUES
('66666665F', 'Rebeca');
INSERT INTO Futbol_personas VALUES
('77777779G','Marta');

INSERT INTO Futbol_personas VALUES
('01111110A',
 'Zinedine Zidane');
INSERT INTO Futbol_personas VALUES
('02222221B',
 'Luis Enrique Martinez Garc�a');
INSERT INTO Futbol_personas VALUES
('03333332C',
 'Diego Simeone');
----- ARBITRO -------

INSERT INTO Futbol_arbitros VALUES
('11111111A', 10);

INSERT INTO Futbol_arbitros VALUES
('22222222B', 2);

INSERT INTO Futbol_arbitros VALUES
('33333333C', 5);

INSERT INTO Futbol_arbitros VALUES
('44444444D', 1);

INSERT INTO Futbol_arbitros VALUES
('55555555E', 3);

INSERT INTO Futbol_arbitros VALUES
('66666666F', 15);

INSERT INTO Futbol_arbitros VALUES
('77777777G', 3);

------ Entrenador ------

INSERT INTO Futbol_Entrenadores VALUES
('01111110A');

INSERT INTO Futbol_Entrenadores VALUES
('02222221B');

INSERT INTO Futbol_Entrenadores VALUES
('03333332C');

------ Equipo --------
INSERT INTO Futbol_Equipos VALUES
('B84030576', 
 'Real Madrid C.F.',
 453000000,
 '01111110A');

INSERT INTO Futbol_Equipos VALUES
('G8266298', 
 'F.C. Barcelona',
 157000000,
 '02222221B');

INSERT INTO Futbol_Equipos VALUES
('A80373764', 
 'Atl�tico de Madrid',
 140000000,
 '03333332C');

----- JUGADOR ------
INSERT INTO Futbol_jugadores VALUES
('11111110A',
 7,
 32000000,
 'Delantero',
 'B84030576');

INSERT INTO Futbol_jugadores VALUES
('22222221B',
 19,
 6760000,
 'CentroCampista',
 'B84030576');

INSERT INTO Futbol_jugadores VALUES
('33333332C',
 2,
 4000000,
 'Defensa',
 'B84030576');

INSERT INTO Futbol_jugadores VALUES
('44444443D',
 3,
 5800000,
 'Defensa',
 'G8266298');

INSERT INTO Futbol_jugadores VALUES
('55555554E',
 7,
 4000000,
 'CentroCampista',
 'G8266298');

INSERT INTO Futbol_jugadores VALUES
('66666665F',
 19,
 1000000,
 'Defensa',
 'A80373764');

INSERT INTO Futbol_jugadores VALUES
('77777779G',
 1,
 3500000,
 'Portero',
 'A80373764');

------ Partido ------
-- Otra opci�n:  ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'DD/MM/YYYY HH24:MI:SS'
INSERT INTO Futbol_Partidos VALUES
(14,
 'Camp Nou',
 TO_DATE('03/12/2016 16:15:00', 'DD/MM/YYYY HH24:MI:SS'),
 'G8266298',
 'B84030576',
 '11111111A');

INSERT INTO Futbol_Partidos VALUES
(11,
 'Vicente Calder�n',
 TO_DATE('19/11/2016 20:45:00', 'DD/MM/YYYY HH24:MI:SS'),
 'A80373764',
 'B84030576',
 '66666666F');

INSERT INTO Futbol_Partidos VALUES
(11,
 'S�nchez Pizjuan',
 TO_DATE('06/11/2016 20:45:00', 'DD/MM/YYYY HH24:MI:SS'),
 'A80373764',
 'G8266298',
 '33333333C');

INSERT INTO Futbol_Partidos VALUES
(24,
 'Vicente Calder�n',
 TO_DATE('25/02/2017 16:15:00', 'DD/MM/YYYY HH24:MI:SS'),
 'A80373764',
 'G8266298',
 '33333333C');

INSERT INTO Futbol_Partidos VALUES
(11,
 'Santiago Bernabeu',
 TO_DATE('25/02/2017 16:15:00', 'DD/MM/YYYY HH24:MI:SS'),
 'A80373764',
 'G8266298',
 '33333333C');

------ Arbitra -------

INSERT INTO Futbol_arbSecundarios VALUES
('11111111A',
 14,
 'Camp Nou');

INSERT INTO Futbol_arbSecundarios VALUES
('66666666F',
 11,
 'Santiago Bernabeu');

INSERT INTO Futbol_arbSecundarios VALUES
('66666666F',
 24,
 'Vicente Calder�n');

------ Acta --------

INSERT INTO Futbol_Actas VALUES
('00000001',
 '11111111A',
 14,
 'Camp Nou');

INSERT INTO Futbol_Actas VALUES
('00000002',
 '66666666F',
 11,
 'Vicente Calder�n');