-- David Dav� Lavi�a

-- 1. Listado de departamentos cuya localizaci�n sea 1500
SELECT * 
FROM pr3_departments
WHERE location_id = 1500;

-- 2. Listado con los nombres de los empleados  que trabajan en el departamento cuyo identificador es 100
SELECT first_name, last_name 
FROM pr3_employees 
WHERE department_id = 100;

-- 3. Listado de los nombres de los empleados que no tienen jefe
SELECT first_name, last_name 
FROM pr3_employees 
WHERE manager_id IS NULL;

-- 4. Identificadores de departamento de aquellos empleados que reciben alg�n tipo de comisi�n. Distinct
-- TODO: Preguntar si se puede hacer con GROUP BY / HAVING
SELECT DISTINCT department_id 
FROM pr3_employees 
WHERE commission_pct > 0 and department_id IS NOT NULL;

-- SELECT department_id FROM pr3_employees GROUP BY (department_id) HAVING commission_pct IS NOT NULL;

-- 5. Nombres de los empleados (order by apellido) que trabajan en el departamento 'Finance'
SELECT first_name, last_name 
FROM pr3_employees JOIN pr3_departments USING (department_id) 
WHERE department_name = 'Finance' ORDER BY last_name;

-- 6. Nombres de los empleados que tienen personal a su cargo, es decir, que son jefes de alg�n empleado, sin repetici�n
SELECT first_name, last_name 
FROM pr3_employees 
WHERE employee_id IN (SELECT manager_id FROM pr3_employees);

-- 7. Listado de los apellidos de los empeados que ganan m�s que su jefe, incluyendo el apellido de su jefe y los salarios de ambos
SELECT empleados.last_name,jefe.last_name,empleados.salary,jefe.salary 
FROM pr3_employees empleados JOIN pr3_employees jefe ON  empleados.manager_id = jefe.employee_id
WHERE empleados.salary > jefe.salary;

-- 8. Listado con los nombres de los empleados que han trabajado en el departamento Sales
SELECT first_name,last_name 
FROM pr3_employees 
WHERE employee_id IN 
    (SELECT employee_id 
     FROM pr3_job_history JOIN pr3_departments USING (department_id) 
     WHERE department_name = 'Sales'); 

-- 9. Nombres de los puestos que desempe�an los empleados en el departamento IT, sin tuplas repetidas
SELECT job_title 
FROM pr3_jobs 
WHERE job_id IN 
    (SELECT job_id 
     FROM pr3_job_history JOIN pr3_departments USING (department_id) 
     WHERE department_name = 'IT');

-- 10. Listado con los nombres de los empleados que trabajan en el departamento IT que no trabajan en Europa,
--     junto con el nombre del pa�s en el que trabajan
SELECT first_name,last_name,country_name
FROM pr3_employees JOIN pr3_departments USING (department_id) JOIN pr3_locations USING (location_id) JOIN pr3_countries USING (country_id)
WHERE region_id IN (SELECT region_id FROM pr3_regions WHERE region_name != 'Europe');

-- 11. Listado de los apellidos de los empleados del departamento SALES que no trabajan en el mismo departamento
--     que su jefe, junto con el apellido de su jefe y el departamento en el que trabaja el jefe
SELECT empleados.last_name,jefe.last_name,jefe.department_id 
FROM pr3_employees empleados JOIN pr3_employees jefe ON empleados.manager_id = jefe.employee_id
WHERE empleados.department_id IN (SELECT department_id FROM pr3_departments WHERE department_name = 'Sales')
    AND NOT empleados.department_id = jefe.department_id;

-- 12. Listado con los nombres de los empleados que han trabajado en el departamento IT, pero
--     que actualmente trabajan en otro departamento distinto
SELECT first_name,last_name 
FROM pr3_employees 
WHERE employee_id IN 
    (SELECT employee_id 
     FROM pr3_job_history JOIN pr3_departments USING (department_id) 
     WHERE department_name = 'IT')
     AND department_id IN (SELECT department_id FROM pr3_departments WHERE department_name != 'IT');

