-- David Davó Laviña

-- 1. Código y nombre de los pilotos certificados para pilotar aviones Boening
SELECT eid, nombre
FROM pr5c1_empleado
WHERE eid IN (
    SELECT eid
    FROM pr5c1_certificado JOIN pr5c1_avion USING (aid)
    WHERE UPPER(nombre) LIKE 'BOEING%'
);

-- 2. Código de los aviones que pueden hacer el recorrido de Los Ángeles a Chicago sin repostar
SELECT aid
FROM pr5c1_avion
WHERE autonomia > (
    SELECT distancia
    FROM pr5c1_vuelo
    WHERE origen = 'Los Angeles' AND destino = 'Chicago'
);

-- 3. Pilotos certificados para operar con aviones con una autonomía superior a 3000 millas pero no
-- certificados para aviones boening
SELECT *
FROM pr5c1_empleado
WHERE eid IN (
    SELECT eid
    FROM pr5c1_certificado JOIN pr5c1_avion USING (aid)
    WHERE autonomia > 3000
) 
AND eid NOT IN (
    SELECT eid
    FROM pr5c1_certificado JOIN pr5c1_avion USING (aid)
    WHERE upper(nombre) LIKE 'BOEING%'
);

-- 4. Empleados con el salario más elevado
SELECT *
FROM pr5c1_empleado
WHERE salario = (
    SELECT max(salario)
    FROM pr5c1_empleado
);

-- 5. Empleados con el segundo salario más alto
SELECT *
FROM pr5c1_empleado
WHERE salario = (
    SELECT max(salario)
    FROM pr5c1_empleado
    WHERE salario <> (
        SELECT max(salario) from pr5c1_empleado
    )
);

-- 6. Empleados con el mayor número de certificaciones para volar
SELECT eid, nombre, count(*) AS certificaciones
FROM pr5c1_empleado JOIN pr5c1_certificado USING (eid)
GROUP BY eid, nombre
HAVING count(*) = (
    SELECT max(count(*))
    FROM pr5c1_empleado JOIN pr5c1_certificado USING (eid)
    GROUP BY eid
);

-- 7. Empleados certificados para 3 modelos de avión
SELECT eid,nombre,count(*) AS certificaciones
FROM pr5c1_empleado JOIN pr5c1_certificado USING(eid)
GROUP BY eid,nombre
HAVING count(*) = 3;

-- 8. Nombre de los aviones tales que todos los pilotos certificados para operar con ellos tengan
-- salarios superiores a 80.000
SELECT nombre
FROM pr5c1_avion
WHERE aid IN (
    SELECT aid
    FROM pr5c1_empleado JOIN pr5c1_certificado USING (eid)
    GROUP BY (aid)
    HAVING min(salario) > 80000
);

-- 9. Para cada piloto certificado para operar con más de 3 modelos de avión indicar el código
-- del empleado y la autonomía máxima de los aviones que puede pilotar
SELECT eid, max(autonomia)
FROM pr5c1_empleado JOIN pr5c1_certificado USING(eid) JOIN pr5c1_avion USING (aid)
GROUP BY eid
HAVING count(aid) >= 3;

-- 10. Nombre de los pilotos cuyo salario es inferior a la ruta más barata entre Los Ángeles y Honolulu
SELECT nombre
FROM pr5c1_empleado
WHERE salario < (
    SELECT min(precio)
    FROM pr5c1_vuelo
    WHERE origen = 'Los Angeles' AND destino = 'Honolulu'
);

-- 11. Mostrar el nombre de los aviones con autonomia superior a 1000 millas junto con la media
-- salarial de los pilotos certificados en dichos aviones
SELECT pr5c1_avion.nombre, avg(salario)
FROM pr5c1_avion JOIN pr5c1_certificado USING (aid) JOIN pr5c1_empleado USING (eid)
WHERE autonomia > 1000
GROUP BY pr5c1_avion.nombre;

-- 12. Calcular la diferencia entre la media salarial de todos los empleados (incluidos los pilotos) y la 
-- de los pilotos
SELECT avg(emp.salario) - avg(pil.salario)
FROM pr5c1_empleado emp, pr5c1_empleado pil
WHERE pil.eid IN (SELECT eid FROM pr5c1_certificado);

-- 13. Listar el nombre y los salarios de los empleados no pilotos cuyo salario sea superior a la media
-- salarial de los pilotos
SELECT nombre, salario
FROM pr5c1_empleado 
WHERE eid NOT IN (
    SELECT eid
    FROM pr5c1_certificado
)
AND salario > (
    SELECT avg(salario)
    FROM pr5c1_empleado
    WHERE eid IN ( SELECT eid FROM pr5c1_certificado )
);

-- 14. Nombre de los pilotos certificados solo para modelos con autonomia superior a 1000 millas
SELECT emp.nombre
FROM pr5c1_empleado emp JOIN pr5c1_certificado USING (eid) JOIN pr5c1_avion USING (aid)
GROUP BY emp.nombre
HAVING min(autonomia) > 1000;
    